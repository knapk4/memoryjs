import './styles.scss';

const student = ["Greg", "Anthony", "Florent", "Loic", "Jonathan", "Rabe", "Sylvain", "Julien", "Greg", "Anthony", "Florent", "Loic", "Jonathan", "Rabe", "Sylvain", "Julien"];

let begin = document.getElementById("start");
const contenu = Array.from(document.getElementsByClassName("contenu"));

// begin.addEventListener('click', function () {
//     let timer = new Date().getTime()+ 60000;
//     let interval = setInterval(function() {
//         let now = new Date().getTime();
//         let distance = timer - now;
//         let seconds = Math.floor((distance % (1000 * 60)) / 1000);
//         document.getElementById("time").innerHTML =seconds + "s ";
//             if (distance < 0) {
//             clearInterval(interval);       
//             alert('Game Over');
//             }
//         }, 1000);
// })

let shuffle = function (array) {
	let currentIndex = array.length;
	let temporaryValue, randomIndex;
	// While there remain elements to shuffle...
	while (0 !== currentIndex) {
		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;
		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}
	return array;
};

begin.addEventListener('click', function() {
    let shuffleStudents = shuffle(student);
    contenu.forEach((card, index) => {
        card.innerHTML = shuffleStudents[index];      
    })
    cacherCarte()
});

const cards = Array.from(document.getElementsByClassName("card"));

function cacherCarte() {
    contenu.forEach(element => element.style.visibility = "hidden");
    cards.forEach(element => element.addEventListener('click', function(event) {      
        event.target.querySelector('p').style.visibility = "visible"
    }))
}

cacherCarte();
